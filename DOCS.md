# Build and Execute app

```
# the user running the environment need to have UID 1000, if not, the node container will give a permission error.
# this can be fixed changing the UID of the node user inside the container, need to match the UID of the user running
# the environment
cd docker
docker-compose build
docker-compose up -d && docker-compose logs -f
```

# Test via curl
```
curl -k -s https://localhost/public | jq
```

# Credential /private

```
# password can be changed in docker/proxy/bin/start.sh
u: admin
p: 1q2w3e
```
